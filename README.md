goagent-startup
===============

Startup script for GoAgent client

Usage
-----
1. Export the environment variable `$GOAGENT_HOME` which points at your GoAgent directory.
2. `./goagent {start|stop|restart|status}`
